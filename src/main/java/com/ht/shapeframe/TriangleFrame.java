/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ACER
 */
public class TriangleFrame {
    public static void main(String[] args) {       
        
       JFrame frame = new JFrame("Triangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblBase = new JLabel("base = " , JLabel.TRAILING);
       lblBase.setSize(50,20);
        lblBase.setLocation(5,5);
        lblBase.setBackground(Color.CYAN);
        lblBase.setOpaque(true);
        frame.add(lblBase);
        
        final JTextField txtBase = new JTextField();
        txtBase.setSize(50,20);
        txtBase.setLocation(60,5);
        frame.add(txtBase);
        
        JLabel lblHeight = new JLabel("height = " , JLabel.TRAILING);
       lblHeight.setSize(50,20);
        lblHeight.setLocation(120,5);
        lblHeight.setBackground(Color.CYAN);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);  
        
        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50,20);
        txtHeight.setLocation(170,5);
        frame.add(txtHeight);        
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(200,5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Triangle base = ??? height = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.YELLOW);
        lblResult.setOpaque(true);
        frame.add(lblResult);        

       btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                String strBase = txtBase.getText();
                double base = Double.parseDouble(strBase);
                String strHeight = txtHeight.getText();
                double height = Double.parseDouble(strHeight);
                Triangle triangle = new Triangle(base,height);
                lblResult.setText("Triangle base = " + String.format("%.2f",triangle.getBase())
                        + "height = " + String.format("%.2f",triangle.getHeight())
                        + " area = " + String.format("%.2f",triangle.calArea())
                        + " perimeter = " +  String.format("%.2f",triangle.calPerimeter()));   
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please Input Number","Error" , JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }
       });
       
        frame.setVisible(true);
    }

}
