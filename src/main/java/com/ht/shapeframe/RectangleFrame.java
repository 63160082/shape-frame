/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ACER
 */
public class RectangleFrame {
    public static void main(String[] args) {
        
       JFrame frame = new JFrame("Rectangle");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblWidth = new JLabel("width = " , JLabel.TRAILING);
       lblWidth.setSize(50,20);
        lblWidth.setLocation(5,5);
        lblWidth.setBackground(Color.ORANGE);
        lblWidth.setOpaque(true);
        frame.add(lblWidth);
        
        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(50,20);
        txtWidth.setLocation(60,5);
        frame.add(txtWidth);
        
        JLabel lblHeight = new JLabel("height = " , JLabel.TRAILING);
       lblHeight.setSize(50,20);
        lblHeight.setLocation(120,5);
        lblHeight.setBackground(Color.ORANGE);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);  
        
        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50,20);
        txtHeight.setLocation(170,5);
        frame.add(txtHeight);        
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(200,5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Rectangle width = ??? height = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.YELLOW);
        lblResult.setOpaque(true);
        frame.add(lblResult);        
        
       btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                String strWidth = txtWidth.getText();
                double width = Double.parseDouble(strWidth);
                String strHeight = txtHeight.getText();
                double height = Double.parseDouble(strHeight);
                Rectangle rectangle = new Rectangle(width,height);
                lblResult.setText("Triangle base = " + String.format("%.2f",rectangle.getWidth())
                        + "height = " + String.format("%.2f",rectangle.getHeight())
                        + " area = " + String.format("%.2f",rectangle.calArea())
                        + " perimeter = " +  String.format("%.2f",rectangle.calPerimeter()));   
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please Input Number","Error" , JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();                    
                }
            }
       });

        frame.setVisible(true);
        
    }
}
